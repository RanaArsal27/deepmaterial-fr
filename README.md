The company has passed ISO9001 quality management system certification and ISO14001 environmental management system certification, and passed the GB/T 29290 intellectual property management system certification, and is in the process of TS16949 management system certification.

In the future, the company will continue to achieve compliance and standardization of the management system.

The company applies for more than 50 invention patents and utility model patents each year, maintain the company's market leadership and technological innovation through continuous technological innovation.

The company is a key introduction project in Guixi City, Jiangxi Province, and has been invested by the State-owned Assets Supervision and Administration Commission.
The company's industrial park base in Guixi occupies an area of 110 acres, with a construction area of 30,000 square meters in the first phase. There is an A1 adhesive plant and a T1 film manufacturing plant.
The company has 1,000 square meters of R&D and sales center and 1,500 square meters of adhesive manufacturing center in Shenzhen.
The company has business service centers and channel service providers in Suzhou, Xiamen, Chengbu, Beijing and Taiwan.

The product is first based on stability and innovation, and the service is first based on integrity. Market-led, technological innovation. Capital blessing to comply with the trend of localization of new materials. Branded operation, insisting on giving priority to value and quality

Become a domestic leader in the bonding and protection of high-end materials for semiconductors and electronic products in China. The company is a key introduction project in Guixi City, Jiangxi Province, and has been invested by the State-owned Assets Supervision and Administration Commission.

Source: [DeepMaterialFr](https://www.deepmaterialfr.com/)
